#
#
FROM node:lts-alpine3.16
# Create app directory
WORKDIR /app
COPY . .

RUN npm i
CMD ["node", "app.js"]
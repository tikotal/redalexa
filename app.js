const https = require('https');

const triggerActivationString = process.env.TRIGGER_ACTIVATION;
const triggerURL = "https://www.virtualsmarthome.xyz/" + triggerActivationString;
const city = process.env.CITY || "";
const inDebug = (process.env.DEBUG || "false") === "true";
var interval = process.env.QUERY_INTERVAL || 2000;
if (interval < 1000)
    interval = 1000;

// changed test object to string to simulate malformed json format tests
const debugResponse = '☼►◄{"id": 1621200679424,"cat": "1","data": ["בית הגדי","נתיבות","מעגלים גבעולים מלילות","שיבולים","שרשרת","רמת גן - מערב","גילת"],"title": "התרעות פיקוד העורף","desc" : "היכנסו למרחב המוגן ושהו בו 10 דקות"}';


let lastID = -1;
let errorCount = 0;

function now() {
    return new Date().toLocaleString();
}

function get(url, options) {
    return new Promise((resolve, reject) => {

        https.get(url, options, (response) => {
            if (response.statusCode == 200) {
                let raw = "";
                // called when a data chunk is received.
                response.on('data', (chunk) => {
                    raw += chunk;
                });

                // called when the complete response is received.
                response.on('end', () => {
                    return resolve(raw);
                });
            } else {
                return reject(new Error(`Invalid response - Status Code: ${response.statusCode}`));
            };
        }).on("error", (error) => {
            return reject(error);
        })
    });
}

function triggerAlexa() {
    const options = {
        headers: {
            'X-Requested-With': 'XMLHttpRequest',
            'Accept': 'application/json, text/javascript, */*; q=0.01',
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36'
        }
    };

    return get(triggerURL, options)
        .then(data => {
            return (JSON.parse(data));
        })
};

function getAlerts() {

    const url = "https://www.oref.org.il/WarningMessages/Alert/alerts.json";
    const options = {
        headers: {
            'X-Requested-With': 'XMLHttpRequest',
            'Referer': 'https://www.oref.org.il/',
            'Host': 'www.oref.org.il',
            'Accept': 'application/json, text/javascript, */*; q=0.01',
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36'
        }
    }

    return get(url, options)
        .then(data => {
            if (inDebug) {
                data = debugResponse;
            };

            if ((data === "") || (data === '﻿\r\n')) {
                return null
            } else {
                // Find the index of the first occurrence of '{' in the 'data' string
                const index = data.indexOf("{");
                 // If '{' is not found in 'data' string, log an error message once per hour and return null
                if (index === -1) {
                    const shouldLogError = (errorCount === 0);
                    if (shouldLogError) {                        
                        console.log(`${now()}: Response Error: ${data}`);
                    }
                    errorCount = (errorCount + 1) % 1800; // 1 hour if interval is 2 seconds
                    return null;
                }

                data = data.substring(index);
                return JSON.parse(data);
            }
        })
}

function main() {

    console.log(`${now()}: Service started...`);
    if (inDebug) {
        console.log("Debug mode.");
    }
    console.log(`Tracking Cities: ${city}`);

    setInterval(() => {
        getAlerts()
            .then((res) => {
                if ((res) && (res.id != -1) && (res.id != lastID)) {
                    lastID = res.id;
                    console.log(`${now()}: Incoming: ${JSON.stringify(res)}`);
                    let matches = res.data.find(s => s.includes(city));

                    if (matches) {
                        console.log(`${now()}: Trigger Alexa on: ${matches}`);
                        return triggerAlexa();
                    }
                }
            })
            .then(trigger => {
                if (!trigger) return;

                if (trigger.URLRoutineTrigger.triggerActivationStatus === "success") {
                    console.log(`${now()}: Trigger sent. ${JSON.stringify(trigger)}`);

                } else {
                    console.log(`${now()}: Trigger failed. ${JSON.stringify(trigger)}`);
                }

            })
            .catch(error => {
                if (error.error)
                    console.log(`${now()}: ${error.error}`);
                else
                    console.log(`${now()}: ${error.message}`);
            })
    }, interval);
}

main();

# README #

This repository is a nodejs service that allows you to connect Red Alert (פיקוד העורף) to Alexa, 
The service pulls the Missile Alert service from oref.org.il and checks if the city of interest is on the list.
if the city was alarmed, the service will trigger a virtual routine using VirtualSmartHome service.

### How do I get set up? ###

* 3 Environment variables are Key (Check launch.json for examples)
*    TRIGGER_ACTIVATION - This is the URL that you will receive from VirtualSmartHome (see below), remove the host part (https://www.virtualsmarthome.xyz/) as this is generic.
*    CITY - which city (currently single one) would you like to trigger the alert on
*    DBEUG - for test purposes, simulates a response which should trigger an alert

### Setup VirtualSmartHome routine trigger
* Setup a virtual trigger using https://www.virtualsmarthome.xyz/url_routine_trigger/

### How to run the image
#### Pull the image
docker pull tikotzki/redalexa:latest

#### Create an environment file 
* Create an environment file env.list and specify the routine trigger url in TRIGGER_ACTIVATION, make sure you copy the json format url
* specify the city you wish to track
      * File will look like:
            - TRIGGER_ACTIVATION=url_routine_trigger/activate.php?trigger=.......................................&response=json
            - CITY=שם העיר
            - DEBUG=false

#### Run the image
* docker run --restart=unless-stopped --env-file ./env.list tikotzki/redalexa

## Contribute

Visit: https://bitbucket.org/tikotal/redalexa